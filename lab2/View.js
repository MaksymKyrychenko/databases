const {Pool, Client} = require('pg');
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'OnlineStores',
    password: null,
    port: 5432
});

const Controller = require('./Controller');
const controller = new Controller(pool);
controller.show()
