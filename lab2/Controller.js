/*const {Pool} = require('pg');
const _pool = new Pool({
    user: process.env.USER,
    host: 'localhost',
    database: 'OnlineStores',
    password: 'Max095433',
    port: 5432
});*/

const {Model, findItems} = require('./Models');
const readLineSync = require('readline-sync');

function nullCheck(string, can_be_null)
{
    if(string.length === 0 && can_be_null === true)
    {
        return null;
    }
    if(string.length === 0 && can_be_null === false)
    {
        let newString = string;
        while(newString.length === 0)
        {
            newString = readLineSync.question("This field can`t be null. Try again");
        }
        return newString
    }
    if(string.length != 0)
    {
        return string;
    }
}

function numberCheck(string)
{
    let num;
    while (isNaN(num = parseInt(string)))
    {
        string = readLineSync.question("\nThis field must be a number! Try again: \n");
    } 
    return num;
}

function dateCheck(string)
{
    const date = new Date(string);
    let newDate = date;
    if(!isNaN(date.getTime()))
    {
        return date;
    }
    while(isNaN(newDate.getTime()))
    {
        const anotherDate = readLineSync.question("Incorrect date. Try again: ");
        newDate = new Date(anotherDate);
    }
    return newDate;
}

class Controller
{
    constructor(_pool)
    {
        this.stores = new Model("OnlineStores", "storeName", _pool);
        this.products = new Model("Products", "EANcode", _pool);
        this.promotions = new Model("SpecialPromotions", "promotionTerms", _pool);
        this.customers = new Model("Customers", "id", _pool);
        this.suppliers = new Model("Suppliers", "supplierName", _pool);
        this.stores_customers = new Model("Stores/Customers", ["storeName", "id"], _pool);
        this.stores_promotions = new Model("Stores/Promotions", ["storeName", "promotionTerms"], _pool);
        this.stores_products = new Model("Stores/Products", ["storeName", "EANcode"], _pool);
        this.stores_suppliers = new Model("Stores/Suppliers", ["storeName", "supplierName"], _pool);
        this.promotions_products = new Model("Promotions/Products", ["promotionTerms", "EANcode"], _pool);
        this.customers_products = new Model("Customers/Products", ["id", "EANcode"],_pool);
    }

    async show()
    {
        while(true)
        {
            let input = readLineSync.question('Enter command:\n1 - to add new item\n2 - to update item\n3 - to delete item\n4 - to create random data\n5 - to find items by conditions\n6 - to exit\n');
            if(input == 1)
            {
                input = 0;
                input = parseInt(readLineSync.question('What kind of item would you like to create:\n1 - OnlineStore\t2 - Product\t3 - SpecialPromotion\n\
                4 - Customer\t5 - Supplier\n6 - Store/Customer con.\t7 - Store/Product con.\n8 - Store/Supplier con.\t 9 - Store/Promotion con.\n\
                10 - Promotion/Product con.\t11 - Customer/Product con.\n'));
                if(input == 1)
                {
                    let params = [];
                    let name = readLineSync.question("Enter storeName (text): ");
                    name = nullCheck(name, false);
                    params[0] = name;
                    let date = readLineSync.question("Enter date of open (any format): ");
                    date = nullCheck(date, false);
                    date = dateCheck(date);
                    params[1] = date;
                    let income = readLineSync.question("Enter its income(number): ");
                    income = nullCheck(income);
                    income = numberCheck(income);
                    params[2] = income;
                    await this.stores.addItem(["storeName", "dateOfOpen", "income"], params);
                    continue;
                }
                if(input == 2)
                {
                    let params = [];
                    let eanCode = readLineSync.question("Enter EANcode (13 digits): ");
                    while(eanCode.length != 13)
                    {
                        eanCode = readLineSync.question("Incorrect code. It must contain 13 digits. Try again: ");
                    }
                    params[0] = numberCheck(eanCode);
                    let suppName = readLineSync.question("Enter supplier name (text): ");
                    params[1] = nullCheck(suppName, false);
                    let priceStr = readLineSync.question("Enter price(number): ");
                    priceStr = nullCheck(priceStr, true);
                    if(priceStr === null)
                    {
                        params[2] = priceStr;
                    }
                    else
                    {
                        priceStr = numberCheck(priceStr);
                        params[2] = priceStr;
                    }
                    let title = readLineSync.question("Enter its title(text): ");
                    title = nullCheck(title, false);
                    params[3] = title;
                    let quantityStr = readLineSync.question("Enter its quantity(number): ")
                    quantityStr = nullCheck(quantityStr);
                    if(quantityStr === null)
                    {
                        params[4] = quantityStr;
                    }
                    else
                    {
                        quantityStr = numberCheck(quantityStr);
                        params[4] = quantityStr;
                    }
                    await this.products.addItem(["EANcode", "supplierName", "price", "title", "quantity"], params);
                    continue;
            }
            if(input == 3)
            {
                let params = [];
                let terms = readLineSync.question("Enter promotion terms(text): ");
                terms = nullCheck(terms, false);
                params[0] = terms;
                let runsFrom = readLineSync.question("Enter when it runs from(any format): ");
                runsFrom = nullCheck(runsFrom, false);
                params[1] = dateCheck(runsFrom);
                let validUntill = readLineSync.question("Enter what it's valid untill(Any format): ")
                validUntill = nullCheck(validUntill, true);
                if(validUntill === null)
                {
                    params[2] = null;
                }
                else
                {
                    params[2] = dateCheck(validUntill);
                }
                await this.promotions.addItem(["promotionTerms", "runsFrom", "validUntill"], params);
                continue;
            }
            if(input == 4)
            {
                let params = [];
                let email = readLineSync.question("Enter email(text): ");
                email = nullCheck(email, false);
                params[0] = email;
                let fullname = readLineSync.question("Enter fullname(text): ");
                fullname = nullCheck(fullname, false);
                params[1] = fullname;
                await this.customers.addItem(["email", "fullname"], params);
                continue;
            }
            if(input == 5)
            {
                let params = [];
                let suppName = readLineSync.question("Enter supplier's name(text): ");
                suppName = nullCheck(suppName, false);
                params[0] = suppName;
                let prod_sort = readLineSync.question("Enter sort of product(text): ");
                prod_sort = nullCheck(prod_sort, false);
                params[1] = prod_sort;
                let location = readLineSync.question("Enter location(text): ");
                location = nullCheck(location, true);
                if(location === null)
                {
                    params[2] = null;
                }
                else
                {
                    params[2] = location;
                }
                let employees = readLineSync.question("Enter number of employees(number): ");
                employees = nullCheck(employees, true);
                if(employees === null)
                {
                    params[3] = null;
                }
                else
                {
                    employees = numberCheck(employees);
                    params[3] = employees;
                }
                await this.suppliers.addItem(["supplierName", "sortOfProduct", "location", "numberOfEmployees"], params);
                continue;
            }
            if(input == 6)
            {
                let params = [];
                let storeName = readLineSync.question("Enter store's name(text): ");
                storeName = nullCheck(storeName, false);
                if(await this.stores.doesExist("storeName", storeName))
                {
                    params[0] = storeName;
                }
                else
                {
                    console.log("Store with that name does not exist");
                    continue;
                }
                let id = readLineSync.question("Enter customer's id(number): ");
                id = nullCheck(id, false);
                id = numberCheck(id);
                if(await this.customers.doesExist("id", id))
                {
                    params[1] = id;
                }
                else
                {
                    console.log("Customer with that id does not exist");
                    continue;
                }
                await this.stores_customers.addItem(["storeName", "id"], params);
                continue;
            }
            if(input == 7)
            {
                let params = [];
                let storeName = readLineSync.question("Enter store's name(text): ");
                storeName = nullCheck(storeName, false);
                if(await this.stores.doesExist("storeName", storeName))
                {
                    params[0] = storeName;
                }
                else
                {
                    console.log("Store with that name does not exist");
                    continue;
                }
                let eanCode = readLineSync.question("Enter product's EANcode(number): ");
                eanCode = nullCheck(eanCode, false);
                eanCode = numberCheck(eanCode);
                if(await this.products.doesExist("EANcode", eanCode))
                {
                    params[1] = eanCode;
                }
                else
                {
                    console.log("Product with that EANcode does not exist");
                    continue;
                }
                await this.stores_products.addItem(["storeName", "EANcode"], params);
            }
            if(input == 8)
            {
                let params = [];
                let storeName = readLineSync.question("Enter store's name(text): ");
                storeName = nullCheck(storeName, false);
                if(await this.stores.doesExist("storeName", storeName))
                {
                    params[0] = storeName;
                }
                else
                {
                    console.log("Store with that name does not exist");
                    continue;
                }
                let suppName = readLineSync.question("Enter suppliers's name(text): ");
                suppName = nullCheck(suppName, false);
                if(await this.suppliers.doesExist("supplierName", suppName))
                {
                    params[1] = suppName;
                }
                else
                {
                    console.log("Supplier with that name does not exist");
                    continue;
                }
                await this.stores_suppliers.addItem(["storeName", "supplierName"], params);
            }
            if(input == 9)
            {
                let params = [];
                let storeName = readLineSync.question("Enter store's name(text): ");
                storeName = nullCheck(storeName, false);
                if(await this.stores.doesExist("storeName", storeName))
                {
                    params[0] = storeName;
                }
                else
                {
                    console.log("Store with that name does not exist");
                    continue;
                }
                let terms = readLineSync.question("Enter promotion terms(text): ");
                terms = nullCheck(terms, false);
                if(await this.promotions.doesExist("promotionTerms", terms))
                {
                    params[1] = terms;
                }
                else
                {
                    console.log("Promotions with that terms does not exist");
                    continue;
                }
                await this.stores_promotions.addItem(["storeName", "promotionTerms"], params);
            }
            if(input == 10)
            {
                let params = [];
                let terms = readLineSync.question("Enter promotion terms(text): ");
                terms = nullCheck(terms, false);
                if(await this.promotions.doesExist("promotionTerms", terms))
                {
                    params[1] = terms;
                }
                else
                {
                    console.log("Promotions with that terms does not exist");
                    continue;
                }
                let eanCode = readLineSync.question("Enter product's EANcode(number): ");
                eanCode = nullCheck(eanCode, false);
                eanCode = numberCheck(eanCode);
                if(await this.products.doesExist("EANcode", eanCode))
                {
                    params[1] = eanCode;
                }
                else
                {
                    console.log("Product with that EANcode does not exist");
                    continue;
                }
                await this.promotions_products.addItem(["promotionTerms", "EANcode"], params);                
            }
            if(input == 11)
            {
                let params = [];
                let id = readLineSync.question("Enter customer's id(number): ");
                id = nullCheck(id, false);
                id = numberCheck(id);
                if(await this.customers.doesExist("id", id))
                {
                    params[1] = id;
                }
                else
                {
                    console.log("Customer with that id does not exist");
                    continue;
                }
                let eanCode = readLineSync.question("Enter product's EANcode(number): ");
                eanCode = nullCheck(eanCode, false);
                eanCode = numberCheck(eanCode);
                if(await this.products.doesExist("EANcode", eanCode))
                {
                    params[1] = eanCode;
                }
                else
                {
                    console.log("Product with that EANcode does not exist");
                    continue;
                }
                await this.customers_products.addItem(["id", "EANcode"], params);
            }
            else
            {
                console.log("Incorrect input");
                continue;
            }
        }
        if(input == 2)
        {
            input = 0;
            input = parseInt(readLineSync.question('What kind of item would you like to UPDATE:\n1 - OnlineStore\t2 - Product\t3 - SpecialPromotion\n\
            4 - Customer\t5 - Supplier\n'));
            if(input == 1)
            {
                let key = readLineSync.question("Enter tuple's key(store name): ");
                key = nullCheck(key, false);
                if(!await this.stores.doesExist(this.stores.tableKey, key))
                {
                    console.log("Store with that key does not exist");
                    continue;
                }
                params[0] = key;
                input = readLineSync.question("Would you like to update its date of open?(1 - yes, 0 - no): ");
                if(input == 1)
                {
                    let date = readLineSync.question("Enter date of open (any format): ");
                    date = nullCheck(date, false);
                    date = dateCheck(date);
                    params[1] = date;
                }
                else
                {
                    params[1] = await this.stores.getAttribute("dateOfOpen", key);
                }
                input = readLineSync.question("Would you like to change its income?(1 - yes, 0 - no");
                if(input == 1)
                {
                    let income = readLineSync.question("Enter new data(number): ");
                    income = nullCheck(income, true);
                    if(income == null)
                    {
                        params[2] = null;
                    }
                    else
                    {
                        income = numberCheck(income);
                        params[2] = income;
                    }
                }
                else
                {
                    params[2] = this.stores.getAttribute("income", key);
                }
                await this.stores.updateItem(["storeName", "dateOfOpen", "income"], params);
                continue;
            }
            if(input == 2)
            {
                let key = readLineSync.question("Enter tuple's key(EANcode): ");
                key = nullCheck(key, false);
                key = numberCheck(key);
                if(!await this.products.doesExist(this.products.tableKey, key))
                {
                    console.log("Store with that key does not exist");
                    continue;
                }
                params[0] = key;
                input = readLineSync.question("Would you like to update its supplier name?(1 - yes, 0 - no): ");
                if(input == 1)
                {
                    let suppName = readLineSync.question("Enter new supplier name (text): ");
                    suppName = nullCheck(suppName, false);
                    if(!await this.suppliers.doesExist(this.suppliers.tableName, suppName))
                    {
                        console.log("You can`t add product with this supplier's name(supplier does not exist)");
                        continue;
                    }
                    params[1] = suppName;
                }
                else
                {
                    params[1] = await this.products.getAttribute("supplierName", key);
                }
                input = readLineSync.question("Would you like to change its price?(1 - yes, 0 - no");
                if(input == 1)
                {
                    let price = readLineSync.question("Enter new data(number): ");
                    price = nullCheck(price, true);
                    if(price == null)
                    {
                        params[2] = null;
                    }
                    else
                    {
                        price = numberCheck(price);
                        params[2] = price;
                    }
                }
                else
                {
                    params[2] = await this.products.getAttribute("price", key);
                }
                input = readLineSync.question("Would you like to update its title?(1 - yes, 0 - no): ");
                if(input == 1)
                {
                    let title = readLineSync.question("Enter new title (text): ");
                    title = nullCheck(title, false);
                    params[3] = suppName;
                }
                else
                {
                    params[3] = this.products.getAttribute("title", key);
                }
                input = readLineSync.question("Would you like to change its quantity?(1 - yes, 0 - no");
                if(input == 1)
                {
                    let quantity = readLineSync.question("Enter new data(number): ");
                    quantity = nullCheck(quantity, true);
                    if(quantity == null)
                    {
                        params[4] = null;
                    }
                    else
                    {
                        price = numberCheck(price);
                        params[4] = price;
                    }
                }
                else
                {
                    params[4] = await this.products.getAttribute("quantity", key);
                }
                await this.products.updateItem(["EANcode", "supplierName", "price", "title", "quantity"], params);
                continue;
        }
        if(input == 3)
        {
            let key = readLineSync.question("Enter tuple's key(promotion terms): ");
            key = nullCheck(key, false);
            if(!await this.promotions.doesExist(this.promotions.tableKey, key))
            {
                console.log("Promotions with that key does not exist");
                continue;
            }
            params[0] = key;
            input = readLineSync.question("Would you like to update when it runs from(date)?(1 - yes, 0 - no): ");
            if(input == 1)
            {
                let date = readLineSync.question("Enter date (any format): ");
                date = nullCheck(date, false);
                date = dateCheck(date);
                params[1] = date;
            }
            else
            {
                params[1] = await this.promotions.getAttribute("runsFrom", key);
            }
            input = readLineSync.question("Would you like to change when does it valid untill?(1 - yes, 0 - no");
            if(input == 1)
            {
                let valid = readLineSync.question("Enter new data(date): ");
                valid = nullCheck(income, true);
                if(valid == null)
                {
                    params[2] = null;
                }
                else
                {
                    valid = dateCheck(valid);
                    params[2] = valid;
                }
            }
            else
            {
                params[2] = await this.promotions.getAttribute("validUntill", key);
            }
            await this.promotions.updateItem(["promotionTerms", "runsFrom", "validUntill"], params);
            continue;
        }
        if(input == 4)
        {
            let key = readLineSync.question("Enter tuple's key(id): ");
            key = nullCheck(key, false);
            key = numberCheck(key);
            if(!await this.customers.doesExist(this.customers.tableKey, key))
            {
                console.log("Customer with that key does not exist");
                continue;
            }
            params[0] = key;
            input = readLineSync.question("Would you like to update its email?(1 - yes, 0 - no): ");
            if(input == 1)
            {
                let email = readLineSync.question("Enter new email (text): ");
                email = nullCheck(email, false);
                params[1] = email;
            }
            else
            {
                params[1] = await this.customers.getAttribute("email", key);
            }
            input = readLineSync.question("Would you like to update its fullname?(1 - yes, 0 - no): ");
            if(input == 1)
            {
                let fullname = readLineSync.question("Enter new fullname (text): ");
                fullname = nullCheck(fullname, false);
                params[2] = fullname;
            }
            else
            {
                params[2] = await this.customers.getAttribute("fullname", key);
            }
            await this.customers.updateItem(["id", "email", "fullname"], params);
            continue;
        }
        if(input == 5)
        {
            let key = readLineSync.question("Enter tuple's key(supplierName): ");
            key = nullCheck(key, false);
            if(!await this.suppliers.doesExist(this.suppliers.tableKey, key))
            {
                console.log("Supplier with that name does not exist");
                continue;
            }
            params[0] = key;
            input = readLineSync.question("Would you like to change its sort of product?(1 - yes, 0 - no)");
            if(input == 1)
            {
                let prod_sort = readLineSync.question("Enter new sort of product(text): ")
                prod_sort = nullCheck(prod_sort, false);
                params[1] = prod_sort;
            }
            else
            {
                params[1] = await this.suppliers.getAttribute("sortOfProduct", key);
            }
            input = readLineSync.question("WOuld you like to change its location?(1 - yes, 0 - no): ");
            if(input == 1)
            {
                let location = readLineSync.question("Enter new location(text): ");
                location = nullCheck(location, true);
                if(location == null)
                {
                    params[2] = null;
                }
                else
                {
                    params[2] = location;
                }
            }
            else
            {
                params[2] = await this.suppliers.getAttribute("location", key);
            }
            input = readLineSync.question("Would you like to change its number of employees(1 -yes, 0 - no");
            if(input == 1)
            {
                let employees = readLineSync.question("Enter new number of employees(number): ");
                employees = nullCheck(employees, true);
                if(employees == null)
                {
                    params[3] = null;
                }
                else
                {
                    employees = numberCheck(employees);
                    params[3] = employees;
                }
            }
            else
            {
                params[3] = await this.suppliers.getAttribute("numberOfEmployees", key);
            }
            await this.suppliers.updateItem(["supplierName", "sortOfProduct", "location", "numberOfEmployees"], params);
            continue;
        }
    }
        if(input == 3)
        {
            input = 0;
            input = readLineSync.question('What kind of item would you like to delete:\n1 - OnlineStore\t2 - Product\t3 - SpecialPromotion\n\
            4 - Customer\t5 - Supplier\n6 - Store/Customer con.\t7 - Store/Product con.\n8 - Store/Supplier con.\t 9 - Store/Promotion con.\n\
            10 - Promotion/Product con.\t11 - Customer/Product con.\n');
            if(input == 1)
            {
                let key = readLineSync.question("Enter name of store: ");
                key = nullCheck(key, false);
                if(!await this.stores.doesExist(this.stores.tableKey, key))
                {
                    console.log("Store with that name does not exist");
                    continue;
                }
                else
                {
                    const toDelete = await findItems([["Stores/Customers", "storeName"], ["Stores/Products", "storeName"], ["Stores/Suppliers", "storeName"],
                ["Stores/Promotions", "storeName"]], [[key], [key], [key], [key]]);
                    for(let i = 0; i < 4; i++)
                    {
                        for(let j = 0; j < toDelete[i].length; j++)
                        {
                            if(i == 0)
                            {
                                await this.stores_customers.deleteItem([toDelete[i][j].storeName, toDelete[i][j].id]);
                            }
                            if(i == 1)
                            {
                                await this.stores_products.deleteItem([toDelete[i][j].storeName, toDelete[i][j].EANcode]);
                            }
                            if(i == 2)
                            {
                                await this.stores_suppliers.deleteItem([toDelete[i][j].storeName, toDelete[i][j].supplierName]);
                            }
                            if(i == 3)
                            {
                                await this.stores_promotions.deleteItem([toDelete[i][j].storeName, toDelete[i][j].promotionTerms]);
                            }
                        }
                    }
                    await this.stores.deleteItem(key);
                    continue;
                }
            }
            if(input == 2)
            {
                let key = readLineSync.question("Enter EANcode(13 digits): ");
                key = nullCheck(key, false);
                while(key.length != 13)
                {
                    key = readLineSync.question("EANcode must contain 13 digits! Try again: ");
                }
                key = numberCheck(key);  
                if(!await this.products.doesExist(this.products.tableKey, key))
                {
                    console.log("\nProduct with that EANcode does not exist!\n");
                    continue;
                }
                else
                {
                    const toDelete = await findItems([["Promotions/Products", "EANcode"], ["Stores/Products", "EANcode"], ["Customers/Products", "EANcode"], [key, key, key]]);
                    for(let i = 0; i < 3; i++)
                    {
                        for(let j = 0; j < toDelete[i].length; j++)
                        {
                            if(i == 0)
                            {
                                await this.promotions_products.deleteItem([toDelete[i][j].promotionTerms, toDelete[i][j].EANcode]);
                            }
                            if(i == 1)
                            {
                                await this.stores_products.deleteItem([toDelete[i][j].storeName, toDelete[i][j].EANcode]);
                            }
                            if(i == 2)
                            {
                                await this.customers_products.deleteItem([toDelete[i][j].id, toDelete[i][j].EANcode]);
                            }
                        }
                    }
                    await this.products.deleteItem(key);    
                    continue;       
                }

            }
            if(input == 3)
            {
                const key = readLineSync.question("Enter promotion terms: ");
                key = nullCheck(key, false);
                if(!await this.promotions.doesExist(this.promotions.tableKey, key))
                {
                    console.log("Promotion with that terms does not exist");
                    continue;
                }
                else
                {
                    const toDelete = await findItems([["Promotions/Products", "promotionTerms"], ["Stores/Promotions", "promotionTerms"], [key, key]]);
                    for(let i = 0; i < 2; i++)
                    {
                        for(let j = 0; j < toDelete[i].length; j++)
                        {
                            if(i == 0)
                            {
                                await this.promotions_products.deleteItem([toDelete[i][j].promotionTerms, toDelete[i][j].EANcode]);
                            }
                            if(i == 1)
                            {
                                await this.stores_promotions.deleteItem([toDelete[i][j].storeName, toDelete[i][j].promotionTerms]);
                            }
                        }
                    }
                    await this.promotions.deleteItem(key);
                    continue;
                }
            }
            if(input == 4)
            {
                const key = readLineSync.question("Enter customer's id: ");
                key = nullCheck(key, false);
                key = numberCheck(key);  
                if(!await this.customers.doesExist(this.customers.tableKey, key))
                {
                    console.log("Product with that EANcode does not exist");
                    continue;
                }
                else
                {
                    const toDelete = await findItems([["Stores/Customers", "id"], ["Customers/Products", "id"], [key, key]]);
                    for(let i = 0; i < 2; i++)
                    {
                        for(let j = 0; j < toDelete[i].length; j++)
                        {
                            if(i == 0)
                            {
                                await this.stores_customers.deleteItem([toDelete[i][j].storeName, toDelete[i][j].id]);
                            }
                            if(i == 1)
                            {
                                await this.customers_products.deleteItem([toDelete[i][j].id, toDelete[i][j].EANcode]);
                            }
                        }
                    }
                    await this.customers.deleteItem(key);
                    continue;
                }
            }
            if(input == 5)
            {
                const key = readLineSync.question("Enter supplier's name: ");
                key = nullCheck(key, false);
                if(!await this.suppliers.doesExist(this.suppliers.tableKey, key))
                {
                    console.log("Product with that EANcode does not exist");
                    continue;
                }
                else
                {
                    const toDelete = await findItems([["Stores/Suppliers", "supplierName"], ["Products", "supplierName"], [key, key]]);
                    for(let i = 0; i < 2; i++)
                    {
                        for(let j = 0; j < toDelete[i].length; j++)
                        {
                            if(i == 0)
                            {
                                await this.stores_suppliers.deleteItem([toDelete[i][j].storeName, toDelete[i][j].supplierName]);
                            }
                            if(i == 1)
                            {
                                await this.customers.deleteItem(toDelete[i][j].EANcode);
                            }
                        }
                    }
                    await this.suppliers.deleteItem(key);
                    continue;
                }
            }
            if(input == 6)
            {
                let storeName = readLineSync.question("Enter store's name: ")
                storeName = nullCheck(storeName, false);
                let id = readLineSync.question("Enter customer's id: ");
                id = nullCheck(id, false);
                id = numberCheck(id);
                let key = [storeName, id];
                if(!await this.stores_customers.doesExist(this.stores_customers.tableKey, key))
                {
                    console.log("Connection with this attributes does not exist");
                    continue;
                }
                else
                {
                    await this.stores_customers.deleteItem(key);
                    continue;
                }
            }
            if(input == 7)
            {
                let storeName = readLineSync.question("Enter store's name: ")
                storeName = nullCheck(storeName, false);
                let eanCode = readLineSync.question("Enter EANcode(13 digits): ");
                eanCode = nullCheck(eanCode, false);
                while(eanCode.length != 13)
                {
                    eanCode = readLineSync.question("EANcode must contain 13 digits! Try again: ");
                }
                eanCode = numberCheck(eanCode);
                let key = [storeName, eanCode];
                if(!await this.stores_products.doesExist(this.stores_products.tableKey, key))
                {
                    console.log("Connection with this attributes does not exist");
                    continue;
                }
                else
                {
                    await this.stores_products.deleteItem(key);
                    continue;
                }
            }
            if(input == 8)
            {
                let storeName = readLineSync.question("Enter store's name: ")
                storeName = nullCheck(storeName, false);
                let suppName = readLineSync.question("Enter suppliersName: ");
                suppName = nullCheck(suppName, false);
                let key = [storeName, suppName];
                if(!await this.stores_suppliers.doesExist(this.stores_suppliers.tableKey, key))
                {
                    console.log("Connection with this attributes does not exist");
                    continue;
                }
                else
                {
                    await this.stores_suppliers.deleteItem(key);
                    continue;
                }
            }
            if(input == 9)
            {
                let storeName = readLineSync.question("Enter store's name: ")
                storeName = nullCheck(storeName, false);
                let terms = readLineSync.question("Enter promotion terms: ");
                terms = nullCheck(terms, false);
                let key = [storeName, terms];
                if(!await this.stores_promotions.doesExist(this.stores_promotions.tableKey, key))
                {
                    console.log("Connection with this attributes does not exist");
                    continue;
                }
                else
                {
                    await this.stores_promotions.deleteItem(key);
                    continue;
                }
            }
            if(input == 10)
            {
                let terms = readLineSync.question("Enter promotion terms: ");
                terms = nullCheck(terms, false);
                let eanCode = readLineSync.question("Enter EANcode(13 digits): ");
                eanCode = nullCheck(eanCode, false);
                while(eanCode.length != 13)
                {
                    eanCode = readLineSync.question("EANcode must contain 13 digits! Try again: ");
                }
                eanCode = numberCheck(eanCode);
                let key = [terms, eanCode];
                if(!await this.promotions_products.doesExist(this.promotions_products.tableKey, key))
                {
                    console.log("Connection with this attributes does not exist");
                    continue;
                }
                else
                {
                    await this.promotions_products.deleteItem(key);
                    continue;
                }
            }
            if(input == 11)
            {
                let id = readLineSync.question("Enter customer's id: ");
                id = nullCheck(id, false);
                id = numberCheck(id);
                let eanCode = readLineSync.question("Enter EANcode(13 digits): ");
                eanCode = nullCheck(eanCode, false);
                while(eanCode.length != 13)
                {
                    eanCode = readLineSync.question("EANcode must contain 13 digits! Try again: ");
                }
                eanCode = numberCheck(eanCode);
                let key = [id, eanCode];
                if(!await this.customers_products.doesExist(this.customers_products.tableKey, key))
                {
                    console.log("Connection with this attributes does not exist");
                    continue;
                }
                else
                {
                    await this.customers_products.deleteItem(key);
                    continue;
                }  
            }
        }
        if(input == 4)
        {
            input = 0;
            let quantity = readLineSync.question("How much entities would you like to create: ");
            quantity = nullCheck(quantity, false);
            quantity = numberCheck(quantity);
            console.log(quantity);
            input = readLineSync.question("What kind of data would you like to create:\n1 - OnlineStores\t2 - Products\t3 - SpecialPromotions\n\
            4 - Customers\t5 - Suppliers\n6 - Stores/Customers con.\t7 - Stores/Products con.\n8 - Stores/Suppliers con.\t 9 - Stores/Promotions con.\n\
            10 - Promotions/Products con.\t11 - Customers/Products con.\n");
            if(input == 1)
            {
                await this.stores.createRandomData(quantity);
                continue;
            }
            if(input == 2)
            {
                await this.products.createRandomData(quantity);
                continue;
            }
            if(input == 3)
            {
                await this.promotions.createRandomData(quantity);
                continue;
            }
            if(input == 4)
            {
                await this.customers.createRandomData(quantity);
                continue;
            }
            if(input == 5)
            {
                await this.suppliers.createRandomData(quantity);
                continue;
            }
           /* if(input == 1)
            {
                this.stores.createRandomData(quantity);
            }
            if(input == 1)
            {
                this.stores.createRandomData(quantity);
            }
            if(input == 1)
            {
                this.stores.createRandomData(quantity);
            }
            if(input == 1)
            {
                this.stores.createRandomData(quantity);
            }
            if(input == 1)
            {
                this.stores.createRandomData(quantity);
            }
            if(input == 1)
            {
                this.stores.createRandomData(quantity);
            }*/
        }
        if(input == 5)
        {
            let question = readLineSync.question("Would you like to run an example (press 1) or your data (press 2)? ");
            if(question == 1)
            {
                findItems();
            }
            if(question == 2)
            {
                let tables = readLineSync.question("Enter tables with attributes which you want to find in that format:\n\
                TableName/attributes(with slashes); separate tables with commas: ");
                tables = nullCheck(tables);
                let tablesArr = tables.split(",");
                let attributes = [];
                let i = 0;
                let values = [];
                for(i; i < tablesArr.length; i++)
                {
                    attributes[i] = tablesArr[i].split("/");
                }
                for(let j = 0; j < i; j++)
                {
                    for(let k = 1; k < attributes[j].length; k++)
                    {
                        let value = readLineSync.question(`Enter ${attributes[j][k]}'s value: `);
                        if(k == 1)
                        {
                            values[j] = [];
                            let num = parseInt(value);
                            if(!isNaN(num))
                            {
                                values[j][0] = num;
                            }
                            else
                            {
                                values[j][0] = value;
                            }
                        }
                        else
                        {
                            let num = parseInt(value);
                            if(!isNaN(num))
                            {
                                values[j].push(num);
                            }
                            else
                            {
                                values[j].push(value);
                            }
                        }
                    }
                }
                await findItems(attributes, values);
                continue;
            } 
        }
        if(input == 6)
        {
            break;
        }
    }
}
}

module.exports = Controller;