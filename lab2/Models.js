const {Pool, Client} = require('pg');

async function shell(pool, text, values = "")
{
    console.log(values);
    let result = await pool
        .query(text, values)
        .catch(e => console.error(e.stack));
}

class classModel
{
    constructor(table_name, table_key, _pool)
    {
        this.tableName = table_name;
        this.tableKey = table_key;
        this.pool = _pool
    }

    async doesExist(col_name, item)
    {
        const pool = new Pool({
            user: 'postgres',
            host: 'localhost',
            database: 'OnlineStores',
            password: null,
            port: 5432
        });
        const text = `SELECT * FROM "${this.tableName}" WHERE "${col_name}" = '${item}'`;
        let result = "";
        await pool
        .query(text)
        .then(res => result = res.rows)
        .then(() => pool.end())
        .catch(e => console.error(e.stack));
        if(result == "" || result == "{}" || result == "[ {} ]")
        {
           // console.log("item does not exist");
            return false;
        }
        else
        {
            return true;
        }
    }

    async getAttribute(attribute, key)
    {
        const text = `SELECT "${attribute}" FROM "${this.tableName}" WHERE "${this.tableKey}" = '${key}'`;
        let res = await shell(this.pool, text);
        return res.rows[0];
    }


    async addItem(params, values)
    {
        if(this.tableName === "OnlineStores" || this.tableName === "SpecialPromotions")
        {
            if(await this.doesExist("storeName", values[0]))
            {
                console.log("\nStore with that name already exist!\n");
                return false;
            }
            const text = `INSERT INTO "${this.tableName}"("${params[0]}", "${params[1]}", "${params[2]}") VALUES($1,  $2, $3)`;
            console.log(text);
            console.log(params);
            console.log(values);
            await shell(this.pool, text, values);
        }
        if(this.tableName === "Customers" || this.tableName === "Stores/Suppliers" || this.tableName === "Stores/Promotions" || this.tableName === "Stores/Customers" || 
        this.tableName === "Stores/Products" || this.tableName === "Promotion/Products" || this.tableName === "Customers/Products")
        {
            const text = `INSERT INTO "${this.tableName}"("${params[0]}", "${params[1]}") VALUES($1, $2)`;
            console.log(text);
            console.log(params);
            console.log(values);
            await shell(this.pool, text, values);
        }
        if(this.tableName === "Suppliers")
        {
            const text = `INSERT INTO "${this.tableName}"("${params[0]}", "${params[1]}", "${params[2]}", "${params[3]}") VALUES($1, $2, $3, $4)`;
            console.log(text);
            console.log(params);
            console.log(values);
            await shell(this.pool, text, values)
        }
        if(this.tableName === "Products")
        {
            const text = `INSERT INTO "${this.tableName}"("${params[0]}", "${params[1]}", "${params[2]}", "${params[3]}", "${params[4]}") VALUES($1, $2, $3, $4, $5)`;
            console.log(text);
            console.log(params);
            console.log(values);
            await shell(this.pool, text, values);
        }
    }
    async updateItem(params, newValues)
    {
        if(this.tableName === "OnlineStores" || this.tableName === "Customers" || this.tableName === "SpecialPromotions")
        {
            const text = `UPDATE "${this.tableName}" SET(${params}) = ('$2', '$3') WHERE "${this.tableKey}" = $1`;
            console.log(text);
            console.log(params);
            console.log(newValues);
            await shell(this.pool, text, newValues);
        }
        if(this.tableName === "Suppliers")
        {
            const text = `UPDATE "${this.tableName}" SET(${params}) = ($2, $3, $4) WHERE "${this.tableKey}" = $1`;
            console.log(text);
            console.log(params);
            console.log(values);
            await shell(this.pool, text, newValues);
        }
        if(this.tableName === "Products")    
        {
            const text = `UPDATE "${this.tableName}" SET(${params}) = ($2, $3, $4, $5) WHERE "${this.tableKey}" = $1`;
            console.log(text);
            console.log(params);
            console.log(values);
            await shell(this.pool, text, newValues);
        }
    }
    

    async deleteItem(itemKey)
    {
        let text = "";
        if(this.tableName === "Stores/Customers" || this.tableName === "Stores/Products" || this.tableName === "Stores/Promotions" ||
        this.tableName === "Stores/Suppliers" || this.tableName === "Promotions/Products" || this.tableName === "Customers/Products")
        {
            text = `DELETE FROM "${this.tableName}" WHERE "${this.tableKey[0]}" = $1 AND "${this.tableKey[1]}" = $2`;
        }
        else
        {
            text = `DELETE FROM "${this.tableName}" WHERE "${this.tableKey}" = $1`;
        }
        await shell(this.pool, text, itemKey);
    }

    async createRandomData(quantity)
    {
        if(this.tableName === "OnlineStores")
        {
            const text = `INSERT INTO "${this.tableName}"("storeName", "dateOfOpen", "income") SELECT chr(trunc(65 + random()*25)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int), 
            timestamp '2014-01-10 20:00:00' +random() * (timestamp '2020-01-20 20:00:00' -
            timestamp '2014-01-10 10:00:00'), trunc(random()*100000)::int from generate_series(1, ${quantity})`;
            await shell(this.pool, text);
        }
        if(this.tableName === "Customers")
        {
            const text = `INSERT INTO "${this.tableName}"("email", "fullname") SELECT chr(trunc(65 + random()*25)::int) || 
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(64)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int), 
            chr(trunc(65 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) ||
            chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(32)::int) || chr(trunc(65 + random()*25)::int) ||
            chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) 
            from generate_series(1, ${quantity})`;
           await shell(this.pool, text);
        }
        if(this.tableName === "SpecialPromotions")
        {
            const text = `INSERT INTO "${this.tableName}"("promotionTerms", "runsFrom", "validUntill") SELECT chr(trunc(65 + random()*25)::int) || 
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int), 
            timestamp '2014-01-10 20:00:00' +random() * (timestamp '2020-01-20 20:00:00' -
            timestamp '2014-01-10 10:00:00'),
            timestamp '2014-01-10 20:00:00' +random() * (timestamp '2020-01-20 20:00:00' -
            timestamp '2014-01-10 10:00:00') from generate_series(1, ${quantity})`;
            await shell(this.pool, text);
        }
        if(this.tableName === "Suppliers")
        {
            const text = `INSERT INTO "${this.tableName}"("supplierName", "sortOfProduct", "location", "numberOfEmployees") SELECT chr(trunc(65 + random()*25)::int) || 
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int), 
            chr(trunc(65 + random()*25)::int) || 
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(64)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int), 
            chr(trunc(65 + random()*25)::int) || 
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(64)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int), 
            tranc(random()*100000)::int from generate_series(1, ${quantity})`;
            await shell(this.pool, text);
        }
        if(this.tableName === "Products")
        {
            const text = `INSERT INTO "${this.tableName}"("EANcode", "supplierName", "price", "title", "quantity") SELECT trunc(1000000000000 + random() * 80000000000000)::bigint,
            select "supplierName" FROM "Suppliers" order by random() limit 1, trunc(random()*100000)::int, chr(trunc(65 + random()*25)::int) || 
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65)::int) ||
            chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int) || chr(trunc(65 + random()*25)::int),  tranc(random()*1000)::int
            from generate_series(1, ${quantity})`;
            await shell(this.pool, text);
        }
        if(this.tableName === "");

    }
}

async function findItemsFunction(tables = [["OnlineStores", "storeName", "income"], ["Customers", "id", "fullname"], ["SpecialPromotions", "runsFrom"]],
 values = [["%A", 100000, 100000000], [10, 50, "%b_"], [new Date(Date.parse("2014-03-15 10:35:54.360631")), new Date(Date.now())]])
 {
     let i = 0;
     let k = 0;
     let n = 0;
     let m = 0;
     let j = 1;
     let texts = [];
     for(i = 0; i < tables.length; i++)
     {
         j = 1;
         let text = `SELECT * FROM "${tables[i][0]}" WHERE `; 
         for(j; j < tables[i].length; j++)
         {
             if(j != 1)
             {
                 text += `AND`;
             }
            if(typeof values[n][m] === 'number' || typeof values[n][m] === 'bigint')
            {
                 let str = `"${tables[i][j]}"=${values[n][m]}`;
                 m += 1;
                text += str;
                continue;
            }
            if(typeof values[n][m] === 'string')
            {
                let str = `"${tables[i][j]}" LIKE '${values[n][m]}'`;
                m += 1;
                text += str;
                continue;
            }
            if(typeof values[n][m] === 'object')
            {
                let UTC = values[n][m].toUTCString;
                let str = `"${tables[i][j]}"='${UTC}'`;
                m += 1;
                text += str;
                continue;
            } 
         }
         m = 0;
         n++;
         texts[i] = text;
     }
     const start = new Date().getTime();
     let results = [];
     let objects = [];
     let a = 0;
     const pool = new Pool({
        user: 'postgres',
        host: 'localhost',
        database: 'OnlineStores',
        password: null,
        port: 5432
    });
     for(k; k < tables.length; k++)
     {
        await pool
        .query(texts[k])
        .then(res => results[k] = res.rows)
        .catch(e => console.error(e.stack));
     }
    const end = new Date().getTime();
    console.log(results);
    console.log(`Execution time: ${end - start}ms`);
    for(a; a < tables.length; a++)
    {
        objects.push(results[a]);
    }
    return objects;
}

 const exportedObject = {
    Model: classModel,
    findItems: findItemsFunction
 };

 module.exports = exportedObject;